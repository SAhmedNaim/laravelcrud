@extends('layouts.app')

@section('content')

@if(session('successMsg'))
	<div class="alert alert-success alert-dismissable fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong> {{ session('successMsg') }}
	</div>
@endif

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Student List</h3>
    </div>
    <div class="panel-body">
        
		<div style="overflow-x:auto;">
			<table class="table table-bordered table-striped">
			    <thead>
			        <tr>
			            <th>ID</th>
			            <th>First name</th>
			            <th>Last name</th>
			            <th>Email</th>
			            <th>Phone</th>
			            <th>Action</th>
			        </tr>
			    </thead>
			    <tbody>

					@foreach($students as $student)
						<tr>
				      		<td>{{ $student->id }}</td>
				      		<td>{{ $student->first_name }}</td>
				      		<td>{{ $student->last_name }}</td>
				      		<td>{{ $student->email }}</td>
				      		<td>{{ $student->phone }}</td>
				      		<td>
				      			<a href="{{ route('edit', $student->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true">Edit</i></a> ||
                                                        
                                <form method="POST" action="{{ route('delete', $student->id) }}" id="delete-form-{{ $student->id }}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    
                                </form>
                                                        
                                <button onclick="if(confirm('Are you sure to delete?')){
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $student->id }}').submit();
                                } else {
                                    event.preventDefault();
                                }" class="btn btn-danger btn-xs"><i class="fa fa-trash-o" aria-hidden="true">Delete</i></button>
				      		</td>
				      	</tr>
					@endforeach

			    </tbody>
			</table>
            
			<div class="col-md-4 col-sm-4"></div>
			<div class="col-md-4 col-sm-4" style="text-align: center;">
				{{ $students->links() }}
			</div>
			<div class="col-md-4 col-sm-4"></div>

                    
		</div>

    </div>
    <div class="panel-footer" style="text-align: center;">
    	All rights reserved. {{ @date('Y') }}
    </div>
</div>

@endsection
@extends('layouts.app')

@section('content')

@if($errors->any())
	@foreach($errors->all() as $error)
		<div class="alert alert-danger alert-dismissable fade in">
    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    		<strong>Warning!</strong> {{ $error }}
		</div>
	@endforeach
@endif

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Student List</h3>
    </div>
    <div class="panel-body">
		
		<form method="POST" action="{{ route('update', $student->id) }}">

			{{ csrf_field() }}
		    
		    <div class="form-group">
		          <label for="firstName">First Name</label>
		          <input type="text" class="form-control" id="firstName" name="firstName" value="{{ $student->first_name }}" />
		    </div>

		    <div class="form-group">
		          <label for="lastName">Last Name</label>
		          <input type="text" class="form-control" id="lastName" name="lastName" value="{{ $student->last_name }}" />
		    </div>

		    <div class="form-group">
		          <label for="email">Email</label>
		          <input type="email" class="form-control" id="email" name="email" value="{{ $student->email }}" />
		    </div>

		    <div class="form-group">
		          <label for="phone">Phone</label>
		          <input type="number" class="form-control" id="phone" name="phone" value="{{ $student->phone }}" />
		    </div>
		  
		    <button type="submit" class="btn btn-default">Update Student</button>
		</form>

    </div>
    <div class="panel-footer" style="text-align: center;">
    	All rights reserved. {{ @date('Y') }}
    </div>
</div>

@endsection